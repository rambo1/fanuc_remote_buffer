# -*- coding: utf-8 -*-

"""Top-level package for Fanuc Remote Buffer Interface."""

__author__ = """Taneli Kaivola"""
__email__ = 'dist@ihme.org'
__version__ = '0.1.3'
